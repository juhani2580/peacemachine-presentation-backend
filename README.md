# Peace Machine Presentation - Backend

This application handles all backend functionality of Peace Machine Presentation. Most notably connections to IBM Watson and analysis tools.

## Setup

```
$ npm install
$ npm start
```
